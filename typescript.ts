
// TYPESCRIPT

// * Supporto dei tipi
// * Parametri di funzione opzionali e di default
// * Tipo di ritorno delle funzioni
// * Supporto nella creazione delle classi
// * Aggiunta di decoratori per arricchire classi
// * Enum
// * Interface



// Decoratori

// Modules → @NgModule()
// Components → @Component()
// Services & DI → @Injectable()
// Directive → @Directive()

@NgModule({
  imports:      [ BrowserModule ],
  providers:    [ Logger ],
  declarations: [ AppComponent ],
  exports:      [ AppComponent ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }