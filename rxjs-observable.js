import { of } from 'rxjs';
import { tap, map } from 'rxjs/operators';

// https://rxjs.dev/guide/overview
// https://www.learnrxjs.io/

// * Non fa parte del linguaggio
// * ReactiveX is a combination of the best ideas from the Observer pattern, the Iterator pattern, and functional programming
// * RxJS è l’implementazione di queste api in javascript
// * Promise ++
// * HttpClient → Observable

of(1,2,3)
  .pipe(
    tap(e => console.log(e)),
    map(e => e + 1)
  )
  .subscribe(
    success => console.log(success),
    error => console.error(error),
    () => console.log(`completed`)
  );

