function Foo() {
  this.data = 0;
  
    function myFunc() {
      console.log(this.data);
    }
  
    const arrowFunc = () => {
      console.log(this.data);
    }
}

// ----

const names = ['wes', 'kait', 'lux'];

const fullNames = names.map(function(name) {
  return `${name} bos`;
});

const fullNames2 = names.map((name) => {
  return `${name} bos`;
});
