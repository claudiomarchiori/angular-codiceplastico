const names = ['Bob', 'Fred', 'Benedict'];
const [ cat, dog, alligator ] = names;

console.log(cat); // 'Bob'
console.log(alligator) // 'Benedict'

// ---

const names = {cat: 'Bob', dog: 'Fred', alligator: 'Benedict'};
const { cat, alligator, dog } = names;

console.log(dog); // 'Fred'
console.log(alligator); // 'Benedict'

// ---

const userSettings = {nightMode: true, fontSize: 'large'}

const {
  nightMode = false,
  language = 'en',
  fontSize = 'normal'
} = userSettings

console.log(nightMode) // true
console.log(language) // 'en'
console.log(fontSize) // 'large'

// ---

function sum({ a = 1, b = 2, c = 3 }) {
  return a + b + c
}

sum({b: 10, a: 5}) // 5 + 10 + 3 = 18
